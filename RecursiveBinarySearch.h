#ifndef _BIN_SEARCH_H_
#define _BIN_SEARCH_H_

#include <vector>
#include <iostream>

class RecursiveBinarySearch
{
    public:
        bool search(std::vector<int> vec, int target);
};

#endif