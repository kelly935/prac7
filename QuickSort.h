#ifndef _QUICK_SORT_H_
#define _QUICK_SORT_H_

#include "Sort.h"

class QuickSort : public Sort
{
    public:
        std::vector<int> sort(std::vector<int> list);

    private:
        typedef struct
        {
            std::vector<int> left;
            std::vector<int> right;
            std::vector<int> pivot;
        } vecPair;

        std::vector<int> combineVec(vecPair pair);
        vecPair splitVec(std::vector<int> vec);
};

#endif