#include "QuickSort.h"

std::vector<int> QuickSort::sort(std::vector<int> list)
{
    //If list size is one just return the list
    if(list.size() == 1)
    {
        return list;
    }

    //If list size is two then we just need to check order of those two elements
    if(list.size() == 2)
    {
        if(list[0] < list[1])
        {
            return std::vector<int>{list[0], list[1]};
        }
        else
        {
            return std::vector<int>{list[1], list[0]};
        }
    }

    //Otherwise we need to split the vector using the pivot and check the result
    vecPair pair = splitVec(list);

    //If either pair is empty we should not try to sort again
    if(!pair.left.empty()) pair.left = sort(pair.left);
    if(!pair.right.empty()) pair.right = sort(pair.right);

    //Combine the left, right and the pivot values to contruct the result
    std::vector<int> result = combineVec(pair);
    return result;
}

//We combine in reverse to get smallest to largest
std::vector<int> QuickSort::combineVec(vecPair pair)
{
    std::vector<int> result;

    for(unsigned int i = 0; i < pair.right.size(); i++)
    {
        result.push_back(pair.right[i]);
    }

    for(unsigned int i = 0; i < pair.pivot.size(); i++)
    {
        result.push_back(pair.pivot[i]);
    }

    for(unsigned int i = 0; i < pair.left.size(); i++)
    {
        result.push_back(pair.left[i]);
    }

    return result;
}

QuickSort::vecPair QuickSort::splitVec(std::vector<int> vec)
{
    //Get the pivot value, use at just to be sure that we don't accidently go out of memory
    int pivot = vec.at(2);
    
    //Setup a vec pair to capture values less then and greater than the pivot
    vecPair pair;

    //Check the list to see what values are greater/less than pivot
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        if(vec[i] > pivot)
        {
            pair.left.push_back(vec[i]);
        }
        else if(vec[i] < pivot)
        {
            pair.right.push_back(vec[i]);
        }
        else
        {
            pair.pivot.push_back(vec[i]);
        }
    }

    return pair;
}


