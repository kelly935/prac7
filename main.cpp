#include <iostream>
#include <sstream>
#include "BubbleSort.h"
#include "QuickSort.h"
#include "RecursiveBinarySearch.h"

int main()
{
    //Get line of user input
    std::string input, tmp;
    getline(std::cin, input);

    std::vector<int> vec_in, vec_out;

    std::stringstream stringstream(input);
    while(!stringstream.eof())
    {
        //Clear the tmp string and feed ss into tmp
        tmp = "";
        stringstream >> tmp;
        vec_in.push_back(stoi(tmp));
    }

    BubbleSort b;
    QuickSort q;
    RecursiveBinarySearch r;

    vec_out = q.sort(vec_in);
    bool found = r.search(vec_out, 1);
    if(found)
        std::cout << "true ";
    else
        std::cout << "false ";
    q.vecPrint(vec_out);
    
}