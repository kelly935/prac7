#ifndef _BUBBLESORT_H_
#define _BUBBLESORT_H_

#include <vector>
#include <iostream>
#include "Sort.h"

class BubbleSort : public Sort
{
    public:
        std::vector<int> sort(std::vector<int> list);

    private:
        struct
        {
            int one = 0; //Index to the first element
            int two = 1; //Index to the second element
            int sortedOffset = 0; //As elements get sorted reduce total number needed to be checked

            std::vector<int> storedList; //List stored internally in selector
            //Loads the list into the selector
            void selectorLoadList(std::vector<int> list) {storedList = list; sortedOffset = 0;}
            //Checks to see if a swap should occur and returns if it does and performs the swap if requied
            bool selectorCheckSwap() 
            {
                //If selector is out of bounds then just immediately return
                if((unsigned int) two >= storedList.size() || two < 0)
                {
                    return false;
                }
                if((unsigned int) one >= storedList.size() || one < 0)
                {
                    return false;
                }

                //Check if swap should occur and perform it
                if(storedList[one] > storedList[two])
                {
                    int tmp = storedList[one];
                    storedList[one] = storedList[two];
                    storedList[two] = tmp;

                    return true;
                }

                return false;
            }
            //Resets the selector indexes back to 0 and 1
            void selectorReset() {one = 0; two = 1;}
            //Increment the selector, returns true if at end of list
            bool selectorIncrement() 
            {
                if((unsigned int) two >= storedList.size() - sortedOffset)
                {
                    sortedOffset++;
                    return true;
                }
                
                one++;
                two++;
                return false;                
            }
            std::vector<int> selectorGetList()
            {
                return storedList;
            }
        } selector;
};

#endif