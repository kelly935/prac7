#include "RecursiveBinarySearch.h"

bool RecursiveBinarySearch::search(std::vector<int> vec, int target)
{
    //In the case where vec is empty just return false
    if(vec.size() == 0)
    {
        return false;
    }

    //If the vec size is one then we check that single element
    if(vec.size() == 1)
    {
        //If match return true
        if(target == vec[0])
        {
            return true;
        }

        return false;
    }

    //Otherwise get the midpoint
    int midpoint = vec.size()/2;

    std::vector<int> next;
    
    //Check if midpoint is what we are looking for
    if(target == vec[midpoint])
    {
        return true;
    }

    //Otherwise check if target is smaller
    else if(target < vec[midpoint])
    {
        for(int i = 0; i < midpoint; i++)
        {
            next.push_back(vec[i]);
        }
    }

    //Or greater
    else
    {
        for(unsigned int i = midpoint + 1; i < vec.size(); i++)
        {
            next.push_back(vec[i]);
        }
    }

    //Search respective subarray
    return search(next, target);
    
}