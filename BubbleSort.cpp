#include "BubbleSort.h"

std::vector<int> BubbleSort::sort(std::vector<int> list)
{
    selector.selectorLoadList(list);

    bool sorted = true;
    while(true)
    {
        //Check if the selector should swap elements and do so
        if(selector.selectorCheckSwap())
        {
            //If a swap occured then list is not sorted
            sorted = false;
        }

        //Check if we are at the end of the list otherwise increment the selector
        if(selector.selectorIncrement())
        {
            //If sorted by the time we are at end of list we are done
            if(sorted)
            {
                break;
            } 
            
            //Put the selector back to the start of the list and restart sorted check
            selector.selectorReset();
            sorted = true;
        }

    }

    return selector.selectorGetList();
}
