#ifndef _SORT_H_
#define _SORT_H_

#include <vector>
#include <iostream>

class Sort
{
    public:
        virtual std::vector<int> sort(std::vector<int> list) = 0;
        void vecPrint(std::vector<int> vec)
        {
            for(unsigned int i = 0; i < vec.size(); i++)
            {
                if(i == vec.size() - 1)
                {
                    std::cout << vec[i];
                }
                else
                {
                    std::cout << vec[i] << " ";
                }
            }
            std::cout << std::endl;
        }
};

#endif